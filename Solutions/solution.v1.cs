using System;

using NUnit.Framework;
using NUnitLite;
using System;
using System.Reflection;
using System.Collections.Generic;

// To execute C#, please define "static void Main" on a class
// named Solution.

class Solution
{
    public static int Main(string[] args)
    {
        return new AutoRun(Assembly.GetCallingAssembly())
            .Execute(new String[] { "--labels=All" });
    }



    public class Calculator
    {
        public Dictionary<int, long> FriendPosition = new Dictionary<int, long>();

        public Calculator(long[] P)
        {
            this.FriendPosition = this.BuildFriendPositions(P);
        }

        public Dictionary<int, long> BuildFriendPositions(long[] pos)
        {
            var friendPos = new Dictionary<int, long>();

            for (var i = 0; i < pos.Length; i++)
            {
                friendPos.Add(i, pos[i]);
            }

            return friendPos;
        }

        public int LastPlaceFriend(int friends)
        {
            var minPos = FriendPosition[0];
            var minFriend = 0;

            for (var friend = 1; friend < friends; friend++)
            {
                var currentPos = this.FriendPosition[friend];
                if (currentPos < minPos)
                {
                    minPos = currentPos;
                    minFriend = friend;
                }
            }
            return minFriend;
        }

        public bool AllFriendsAtTheFront(long frontRow)
        {
            foreach (var item in this.FriendPosition)
            {
                if (item.Value != frontRow)
                {
                    return false;
                }
            }

            return true;
        }

        public int? IsFriendOnRow(long row)
        {
            foreach (var item in this.FriendPosition)
            {
                if (item.Value == row)
                {
                    return item.Key;
                }
            }

            return null;
        }

        public long NextFreeRow(int friend, long frontRow)
        {

            for (var nextRow = this.FriendPosition[friend] + 1; nextRow < frontRow; nextRow++)
            {
                if (!this.IsFriendOnRow(nextRow).HasValue)
                {
                    return nextRow;
                }
            }

            return frontRow;
        }

        public long GetSecondsToReachTheFront(long N, int F)
        {

            var frontRow = N;
            var friend = F;

            var steps = 0;
            while (this.AllFriendsAtTheFront(frontRow) == false)
            {
                steps++;
                var targetFriendToBoost = this.LastPlaceFriend(F);
                var moveTo = this.NextFreeRow(targetFriendToBoost, N);
                this.FriendPosition[targetFriendToBoost] = moveTo;

            }


            return steps;
        }


    }

    [TestFixture]
    public class ToTheFrontTests
    {
        public Calculator GetTarget(long[] P)
        {
            return new Calculator(P);
        }


        [Test]
        public void BuildFriendPositions_Should_Build_Single()
        {
            var pos = new long[] { 1 };
            var target = GetTarget(pos);

            var actual = target.BuildFriendPositions(pos);

            Assert.AreEqual(actual[0], 1);
        }

        [Test]
        public void BuildFriendPositions_Should_Build_Three()
        {
            var pos = new long[] { 5, 2, 4 };
            var target = GetTarget(pos);

            var actual = target.BuildFriendPositions(pos);

            Assert.AreEqual(actual[0], 5);
            Assert.AreEqual(actual[1], 2);
            Assert.AreEqual(actual[2], 4);
        }

        [Test]
        public void LastPlaceFriend_Should_Find_SoloFriend()
        {
            var target = GetTarget(new long[] { 5 });

            var actual = target.LastPlaceFriend(1);

            Assert.AreEqual(0, actual);

        }

        [Test]
        public void LastPlaceFriend_Should_Build_three()
        {
            var target = GetTarget(new long[] { 5, 4, 2 });

            var actual = target.LastPlaceFriend(3);

            Assert.AreEqual(2, actual);

        }

        [Test]
        public void AllFriendsHome_Should_Build_three()
        {
            var target = GetTarget(new long[] { 5, 5, 5 });

            var actual = target.AllFriendsAtTheFront(5);

            Assert.AreEqual(true, actual);

        }


        [Test]
        public void IsFriendOnRow_Should_Find_Pos()
        {
            var target = GetTarget(new long[] { 3, 2, 4 });

            var actual = target.IsFriendOnRow(4);

            Assert.AreEqual(2, actual);

        }

        [Test]
        public void IsFriendOnRow_Should_Return_Null()
        {
            var target = GetTarget(new long[] { 3, 2, 4 });

            var actual = target.IsFriendOnRow(1);

            Assert.AreEqual(null, actual);

        }

        [Test]
        public void NextFreeRow_Should_Jump_Ahead_Over_Friend_Three_Pad_Four()
        {
            var target = GetTarget(new long[] { 3, 2, 4 });

            var actual = target.NextFreeRow(0, 6);

            Assert.AreEqual(5, actual);

        }

        [Test]
        public void NextFreeRow_Should_Jump_Ahead_OnlyOnce()
        {
            var target = GetTarget(new long[] { 1, 3, 4 });

            var actual = target.NextFreeRow(0, 6);

            Assert.AreEqual(2, actual);
        }

        [Test]
        public void NextFreeRow_Should_Jump_Home()
        {
            var target = GetTarget(new long[] { 3, 4, 5 });

            var actual = target.NextFreeRow(0, 6);

            Assert.AreEqual(6, actual);
        }

        [Test]
        public void NextFreeRow_Should_Go_Home()
        {
            var frontRow = 6;
            var target = GetTarget(new long[] { 5, 6 });

            var nextFree = target.NextFreeRow(0, frontRow);

            Assert.AreEqual(frontRow, nextFree);

            var allHomeActualBeforemove = target.AllFriendsAtTheFront(frontRow);
            Assert.AreEqual(false, allHomeActualBeforemove);

            target.FriendPosition[0] = nextFree;

            var allHomeActualAfterMove = target.AllFriendsAtTheFront(frontRow);
            Assert.AreEqual(true, allHomeActualAfterMove);
        }




        [Test]
        public void Example_1()
        {
            /*
            N = 3
            F = 1
            P = [1]
            */

            var target = GetTarget(new long[] { 1 });

            var seconds = target.GetSecondsToReachTheFront(3, 1);

            Assert.AreEqual(2, seconds);
        }

        [Test]
        public void Example_2()
        {
            /*
            N = 6
            F = 3
            P = [5, 2, 4]
            */

            var target = GetTarget(new long[] { 5, 2, 4 });

            var seconds = target.GetSecondsToReachTheFront(6, 3);

            Assert.AreEqual(4, seconds);
        }
    }
}
