# ToTheFrontRow

## Getting started

A group of friends at a concert are keen to get to the front row as quickly as possible.

They hope to do so by crowd surfing across rows of their friends in the audience (a trail of rows), numbered from 1 to N in order.

There are **F** friends, numbered from **1** to **F**. Friend **i** is currently at row **Pi**​.

- No two friends are currently on the same row.
- Row **N** is right next to the stage, and none of the friends are initially on the front row **N**.

Each second, one friend may jump forwards towards the front row of **N**.

- When a friend jumps, they move to the nearest row after its current row which is not currently occupied by another friend
- If they reach a row occupied by another friend they getting **boosted** on intermediate rows along the way (bascially crowd surf!)
- If this causes them to reach the front row **N**, they will have reach their goal!
- Multiple friends may not simultaneously jump during the same second.

Assuming the friends work together optimally when deciding which friend should jump during each second, determine the minimum number of seconds required for all **F** of them to reach the front row.

## Example

### Example 1 - Solo Friend

```
N = 3
F = 1
P = [1]
```

Expected Return Value = 2

In the first case, there are 3 rows and 1 friend. The friend is initially on row 1 and will take 2 jumps to reach the front row 3.

### Example 2 - T

```
N = 6
F = 3
P = [5, 2, 4]
```

Expected Return Value = **4**

In the second case, there are 6 rows to the front , with friends on rows 5, 2, and 4.

Initially the rows and friends numbers can be represented as `.2.31.`

- A full stop is empty row (IE there are three empty rows in `.2.31.`)
- A number is a friend so the first index of of **P** (5) is one jump away from the front row **N** of 6

One optimal sequence of jumps is:

    - Friend 2 jumps forward to row 3: `..231`.
    - Friend 2 jumps over friend 1 and 3, onto row 6 and reaches the front row: `...31.`
    - Friend 3 jumps over friend 1, onto row 6 and reaches the front row: `....1.`
    - Friend 1 jumps forward onto rows 6 and also reachs the front row `......`
